package server

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/go-ff/ff"
	"gitlab.com/go-ff/gitlab"
)

//Start is responsible for get the server running
func Start(port int) {
	gin := gin.Default()

	flag := gitlab.New()

	ffHandler := ff.New(flag)
	ffHandler.RegisterRoutes(gin)

	gin.Run(fmt.Sprintf(":%d", port))
}
