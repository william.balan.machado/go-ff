package gitlab

import (
	"os"

	"github.com/Unleash/unleash-client-go"
)

//FeatureFlag encapsulates the logic around feature flags
type FeatureFlag struct {
}

//New creates an instance to FeatureFlag
func New() FeatureFlag {
	unleash.Initialize(
		unleash.WithAppName(os.Getenv("FF_ENV")),
		unleash.WithUrl(os.Getenv("FF_API_URL")),
		unleash.WithInstanceId(os.Getenv("FF_INSTANCE_ID")),
		unleash.WithListener(&FeatureFlag{}),
	)
	return FeatureFlag{}
}

//Check returns true if the flag with the name in the argument is ON
//Returns false otherwise
func (f FeatureFlag) Check(flagName string) bool {
	return unleash.IsEnabled(flagName)
}
