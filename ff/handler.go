package ff

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/go-ff/gitlab"
)

//Handler handles feature flag routes
type Handler struct {
	featureFlag gitlab.FeatureFlag
}

//New creates an instance of this handler
func New(featureFlag gitlab.FeatureFlag) Handler {
	return Handler{
		featureFlag: featureFlag,
	}
}

func (h Handler) handleGetFF(g *gin.Context) {
	flagName := g.Param("ff")
	fmt.Println("Flag name", flagName)
	enabled := h.featureFlag.Check(flagName)

	fmt.Println("Flag enabled", enabled)

	g.JSON(http.StatusOK, gin.H{
		"flag":    flagName,
		"enabled": enabled,
	})
}
