package ff

import "github.com/gin-gonic/gin"

//RegisterRoutes adds routes specific to feature flags
func (h Handler) RegisterRoutes(r *gin.Engine) {
	r.GET("/:ff", h.handleGetFF)
}
