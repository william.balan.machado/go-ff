package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/go-ff/server"
)

var rootCmd = &cobra.Command{
	Use:   "server",
	Short: "Start the server",
	Run:   root,
}

var serverPort int

func init() {
	rootCmd.PersistentFlags().IntVar(&serverPort, "port", 5000, "the port used to start the server")
}

func root(cmd *cobra.Command, args []string) {
	server.Start(serverPort)
}

//Execute starts the root cmd
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		panic("Error executing root cmd")
	}
}
